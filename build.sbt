val http4sVersion = "0.21.5"
val ioCircleVersion = "0.13.0"
val mongoVersion = "2.2.1"
val specs2Version = "4.10.0"
val logbackVersion = "1.2.3"
val mockitoVersion = "1.5.10"

lazy val root = (project in file("."))
  .settings(
    organization := "com.scala",
    name := "example",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.12.12",
    libraryDependencies ++= Seq(
        "org.http4s"        %% "http4s-blaze-server"     % http4sVersion,
        "org.http4s"        %% "http4s-dsl"              % http4sVersion,
        "org.http4s"        %% "http4s-blaze-client"     % http4sVersion,
        "org.http4s"        %% "http4s-circe"            % http4sVersion,
        "org.http4s"        %% "http4s-json4s-jackson"   % http4sVersion,
        "org.http4s"        %% "http4s-json4s-native"    % http4sVersion,
        "io.circe"          %% "circe-generic"           % ioCircleVersion,
        "io.circe"          %% "circe-literal"           % ioCircleVersion,
        "org.mongodb.scala" %% "mongo-scala-driver"      % mongoVersion,
        "org.specs2"        %% "specs2-core"             % specs2Version % "test",
        "org.mockito"       %% "mockito-scala-scalatest" % mockitoVersion,
        "ch.qos.logback"    %  "logback-classic"         % logbackVersion
    )
  )

scalacOptions ++= Seq("-Ypartial-unification")