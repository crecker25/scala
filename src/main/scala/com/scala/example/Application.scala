package com.scala.example

import cats.effect._
import com.scala.example.user.{UserRepositoryImpl, UserRoutes, UserServiceImpl}
import org.http4s.implicits._
import org.http4s.server.blaze._

import scala.concurrent.ExecutionContext.global
import scala.io.Source

object Application extends IOApp {

  private val bannerFileName = "banner.txt"

  def run(args: List[String]): IO[ExitCode] =
    BlazeServerBuilder[IO](global)
      .bindLocal(8080)
      .withHttpApp(new UserRoutes(new UserServiceImpl(new UserRepositoryImpl)).routes().orNotFound)
      .withBanner(Source.fromResource(bannerFileName).getLines.toList)
      .serve
      .compile
      .drain
      .as(ExitCode.Success)
}
