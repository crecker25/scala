package com.scala.example.user

import cats.effect.IO
import com.scala.example.user.UserModels.{Id, User, Username}
import org.mongodb.scala.model.Sorts
import org.mongodb.scala.{Document, Observer}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class UserRepositoryImpl extends UserRepository {

  private val userCollection = DatabaseConfig.getCollection("users")

  private def observer(textOnComplete: String) = new Observer[Any] {
    override def onNext(result: Any): Unit = {}

    override def onError(e: Throwable): Unit = throw e

    override def onComplete(): Unit = println(textOnComplete)
  }

  override def getUserById(userId: Id): IO[Option[User]] = IO {
    val doc: Document = Document("_id" -> userId)
    val userFuture = userCollection.find(doc)
      .map(doc => new User(doc.getString("_id"), doc.getString("username"), doc.getString("email"), doc.getInteger("age")))
      .headOption()

    Await.result(userFuture, Duration(1, java.util.concurrent.TimeUnit.SECONDS))
  }

  override def getUserByUsername(username: Username): IO[Option[User]] = IO {
    val doc: Document = Document("username" -> username)
    val userFuture = userCollection.find(doc)
      .map(doc => new User(doc.getString("_id"), doc.getString("username"), doc.getString("email"), doc.getInteger("age")))
      .headOption()

    Await.result(userFuture, Duration(1, java.util.concurrent.TimeUnit.SECONDS))
  }

  override def getAllUsers(sortDirection: OrderDirection.Value): IO[List[User]] = IO {
    val usersFuture = userCollection
      .find()
      .sort(if (sortDirection == OrderDirection.ASC) Sorts.ascending("username") else Sorts.descending("username"))
      .map(doc => new User(doc.getString("_id"), doc.getString("username"), doc.getString("email"), doc.getInteger("age")))
      .toFuture()

    Await.result(usersFuture, Duration(1, java.util.concurrent.TimeUnit.SECONDS)).toList
  }

  override def createUser(user: User): IO[User] = IO {
    val doc: Document = Document("_id" -> user.id, "username" -> user.username, "email" -> user.email, "age" -> user.age)
    userCollection.insertOne(doc).subscribe(observer("New user has been created"))
    user
  }

  override def updateUser(user: User): IO[User] = IO {
    val docId: Document = Document("_id" -> user.id)
    val doc: Document = Document("_id" -> user.id, "username" -> user.username, "email" -> user.email, "age" -> user.age)
    val updatedUserFuture = userCollection.findOneAndReplace(docId, doc)
      .map(doc => new User(doc.getString("_id"), doc.getString("username"), doc.getString("email"), doc.getInteger("age")))
      .head()

    Await.result(updatedUserFuture, Duration(1, java.util.concurrent.TimeUnit.SECONDS))
  }

  override def deleteUser(userId: Id): Unit = {
    val doc: Document = Document("_id" -> userId)
    userCollection.findOneAndDelete(doc).subscribe(observer(s"User with id: $userId has been deleted"))
  }
}
