package com.scala.example.user

import cats.effect.IO
import com.scala.example.user.UserModels.{Id, User, Username}

trait UserRepository {

  def getUserById(userId: Id): IO[Option[User]]
  
  def getUserByUsername(username: Username): IO[Option[User]]

  def getAllUsers(sortDirection: OrderDirection.Value): IO[List[User]]

  def createUser(user: User): IO[User]

  def updateUser(user: User): IO[User]

  def deleteUser(userId: Id): Unit

}
