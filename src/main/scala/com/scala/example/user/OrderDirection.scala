package com.scala.example.user

object OrderDirection extends Enumeration   {
    val ASC, DESC = Value
}
