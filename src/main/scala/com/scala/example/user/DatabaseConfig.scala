package com.scala.example.user

import org.mongodb.scala._

object DatabaseConfig {

  private val mongoClient: MongoClient = MongoClient("mongodb://localhost:27017")
  private val database: MongoDatabase = mongoClient.getDatabase("scala")

  def getCollection(collectionName: String): MongoCollection[Document] = database.getCollection(collectionName)
}
