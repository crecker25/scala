package com.scala.example.user

import cats.effect.IO
import com.scala.example.user.UserModels.{Id, Username}
import org.http4s.{InvalidMessageBodyFailure, Response}
import org.http4s.Status.{Conflict, NotFound, BadRequest}

object Errors {
  
  val errorHandler: PartialFunction[Throwable, IO[Response[IO]]] = {
    case UserNotFoundException(id) => IO {
      Response(status = NotFound).withEntity(s"User with id: $id not found!")
    }
    case DuplicatedUsernameException(username) => IO {
      Response(status = Conflict).withEntity(s"Username $username already in use!")
    }
    case InvalidMessageBodyFailure(details, cause) => IO {
      Response(status = BadRequest).withEntity(details)
    }
  }
}

case class UserNotFoundException(id: Id) extends RuntimeException {

}

case class DuplicatedUsernameException(username: Username) extends RuntimeException {

}