package com.scala.example.user

import cats.effect.IO
import com.scala.example.user.UserModels._

class UserServiceImpl(val userRepositoryImpl: UserRepositoryImpl) extends UserService {
  
  override def getUserById(userId: Id): IO[UserDto] = userRepositoryImpl.getUserById(userId)
    .map(userOption => userOption.getOrElse(throw UserNotFoundException(userId)))
    .map(user => UserDto(user))

  override def getAllUsers(sortDirection: OrderDirection.Value): IO[List[UserDto]] = userRepositoryImpl.getAllUsers(sortDirection)
    .map(users => users.map(user => UserDto(user)))

  override def createUser(userDtoToCreate: UserDtoToCreate): IO[UserDto] = {
    val userOption: Option[User] = userRepositoryImpl.getUserByUsername(userDtoToCreate.username).unsafeRunSync()
    userOption match {
      case Some(user) => throw DuplicatedUsernameException(user.username)
      case None => userRepositoryImpl.createUser(new User(userDtoToCreate)).map(user => UserDto(user))
    }
  }

  override def updateUser(userId: Id, userDtoToUpdate: UserDtoToUpdate): IO[UserDto] = userRepositoryImpl.getUserById(userId)
    .map(userOption => userOption.getOrElse(throw UserNotFoundException(userId)))
    .flatMap(currentUser => userRepositoryImpl.updateUser(new User(userDtoToUpdate, currentUser)))
    .map(updatedUser => UserDto(updatedUser))

  override def deleteUSer(userId: Id): IO[Unit] = userRepositoryImpl.getUserById(userId)
    .map(userOption => userOption.getOrElse(throw UserNotFoundException(userId)))
    .map(_ => userRepositoryImpl.deleteUser(userId))
}
