package com.scala.example.user

import cats.effect.IO
import com.scala.example.user.UserModels.{UserDtoToCreate, UserDtoToUpdate}
import io.circe.generic.auto._
import org.http4s.circe.CirceEntityCodec._
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.impl.OptionalQueryParamDecoderMatcher
import org.http4s.{HttpRoutes, QueryParamDecoder}

import scala.util.Try


class UserRoutes(userServiceImpl: UserServiceImpl) {
  
  implicit val queryParamDecoder: QueryParamDecoder[OrderDirection.Value] = QueryParamDecoder[String]
    .map(_ toUpperCase)
    .map(sortDirection => Try.apply(OrderDirection.withName(sortDirection))
      .getOrElse(OrderDirection.ASC))

  object OptionalSortOrderQueryParamMatcher extends OptionalQueryParamDecoderMatcher[OrderDirection.Value]("sort")

  def routes(): HttpRoutes[IO] = {

    val dsl = new Http4sDsl[IO] {}
    import dsl._

    HttpRoutes.of[IO] {
      case _@GET -> Root / "users" / id =>
        userServiceImpl.getUserById(id) flatMap (res => Ok(res)) handleErrorWith Errors.errorHandler

      case _@GET -> Root / "users" :? OptionalSortOrderQueryParamMatcher(maybeSortDirection) =>
        maybeSortDirection match {
          case None =>
            userServiceImpl.getAllUsers(OrderDirection.ASC) flatMap (users => Ok(users))
          case Some(sortDirection) =>
            userServiceImpl.getAllUsers(sortDirection) flatMap (users => Ok(users))
        }

      case req@POST -> Root / "users" =>
        req.as[UserDtoToCreate].map(userServiceImpl.createUser) flatMap (res => Created(res)) handleErrorWith Errors.errorHandler

      case req@PUT -> Root / "users" / id =>
        req.as[UserDtoToUpdate].map(userDtoToUpdate => userServiceImpl.updateUser(id, userDtoToUpdate)) flatMap (res => Accepted(res)) handleErrorWith Errors.errorHandler

      case _@DELETE -> Root / "users" / id =>
        userServiceImpl.deleteUSer(id) flatMap (_ => NoContent()) handleErrorWith Errors.errorHandler
    }
  }
}
