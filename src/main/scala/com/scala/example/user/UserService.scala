package com.scala.example.user

import cats.effect.IO
import com.scala.example.user.UserModels.{Id, UserDto, UserDtoToCreate, UserDtoToUpdate}

trait UserService {

  def getUserById(userId: Id): IO[UserDto]

  def getAllUsers(sortDirection: OrderDirection.Value): IO[List[UserDto]]

  def createUser(user: UserDtoToCreate): IO[UserDto]

  def updateUser(userId: Id, user: UserDtoToUpdate): IO[UserDto]

  def deleteUSer(userId: Id): IO[Unit]

}
