package com.scala.example.user

import org.bson.types.ObjectId

object UserModels {

  type Id = String
  type Username = String
  type Email = String
  type Age = Int

  final class User(val id: String, val username: Username, val email: Email, val age: Age) {
    def this(userDtoToCreate: UserDtoToCreate) {
      this(new ObjectId().toString, userDtoToCreate.username, userDtoToCreate.email, userDtoToCreate.age)
    }

    def this(userDtoToUpdate: UserDtoToUpdate, user: User) {
      this(user.id, user.username, user.email, userDtoToUpdate.age)
    }
  }

  final case class UserDto(user: User) {
    var id: Id = user.id
    var username: Username = user.username
    var email: Email = user.email
    var age: Age = user.age
  }

  final case class UserDtoToCreate(username: Username, email: Email, age: Age)

  final case class UserDtoToUpdate(age: Age)

}
