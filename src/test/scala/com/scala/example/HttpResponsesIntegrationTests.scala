package com.scala.example

import cats.effect.IO
import com.scala.example.user.UserModels.{User, UserDtoToCreate, UserDtoToUpdate}
import com.scala.example.user.{UserRepositoryImpl, UserRoutes, UserServiceImpl}
import io.circe.generic.auto._
import org.http4s.circe.CirceEntityCodec._
import org.http4s.implicits._
import org.http4s.{Method, Request, Response, Status, _}
import org.mockito.ArgumentMatchersSugar.any
import org.mockito.MockitoSugar.{spy, when}
import org.specs2.mutable.Specification


class HttpResponsesIntegrationTests extends Specification {

  "Http request tests" >> {
    "When GET all users than should return 200" >> {
      responseFromGet(uri"/users", new UserRepositoryImpl).status must beEqualTo(Status.Ok)
    }
    "When GET user with valid id than should return 200 otherwise 404" >> {
      val userRepositorySpy = spy(new UserRepositoryImpl)
      when(userRepositorySpy.getUserById("1")).thenReturn(IO {
        Some(new User("1", "notunique", "", 1))
      })
      responseFromGet(uri"/users/1", userRepositorySpy).status must beEqualTo(Status.Ok)
      responseFromGet(uri"/users/2", userRepositorySpy).status must beEqualTo(Status.NotFound)
    }
    "When POST with unique name then should return 201 otherwise 409" >> {
      val userRepositorySpy = spy(new UserRepositoryImpl)
      when(userRepositorySpy.getUserByUsername("notunique")).thenReturn(IO {
        Some(new User("1", "notunique", "", 1))
      })
      responseFromPost(uri"/users", new UserRepositoryImpl(), UserDtoToCreate("notunique", "", 1)).status must beEqualTo(Status.Conflict)
    }
    "When PUT user with valid id then should return 202 otherwise 404" >> {
      val userRepositorySpy = spy(new UserRepositoryImpl)
      when(userRepositorySpy.getUserById("1")).thenReturn(IO {
        Some(new User("1", "", "", 1))
      })
      when(userRepositorySpy.updateUser(any[User])).thenReturn(IO {
        new User("1", "", "", 1)
      })
      responseFromPut(uri"/users/1", userRepositorySpy, UserDtoToUpdate(5)).status must beEqualTo(Status.Accepted)
      responseFromPut(uri"/users/2", userRepositorySpy, UserDtoToUpdate(5)).status must beEqualTo(Status.NotFound)
    }
    "When DELETE user with valid id than should return 204 otherwise 404" >> {
      val userRepositorySpy = spy(new UserRepositoryImpl)
      when(userRepositorySpy.getUserById("1")).thenReturn(IO {
        Some(new User("1", "notunique", "", 1))
      })
      responseFsromDelete(uri"/users/1", userRepositorySpy).status must beEqualTo(Status.NoContent)
      responseFsromDelete(uri"/users/2", userRepositorySpy).status must beEqualTo(Status.NotFound)
    }
  }

  private[this] def responseFromGet(uri: Uri, userRepositoryImpl: UserRepositoryImpl): Response[IO] = {
    val getHW = Request[IO](Method.GET, uri)
    new UserRoutes(new UserServiceImpl(userRepositoryImpl)).routes().orNotFound(getHW).unsafeRunSync()
  }

  private[this] def responseFromPost(uri: Uri, userRepositoryImpl: UserRepositoryImpl, userDtoToCreate: UserDtoToCreate): Response[IO] = {
    val getHW = Request[IO](Method.POST, uri).withEntity(userDtoToCreate)
    new UserRoutes(new UserServiceImpl(userRepositoryImpl)).routes().orNotFound(getHW).unsafeRunSync()
  }

  private[this] def responseFromPut(uri: Uri, userRepositoryImpl: UserRepositoryImpl, userDtoToUpdate: UserDtoToUpdate): Response[IO] = {
    val getHW = Request[IO](Method.PUT, uri).withEntity(userDtoToUpdate)
    new UserRoutes(new UserServiceImpl(userRepositoryImpl)).routes().orNotFound(getHW).unsafeRunSync()
  }

  private[this] def responseFsromDelete(uri: Uri, userRepositoryImpl: UserRepositoryImpl): Response[IO] = {
    val getHW = Request[IO](Method.DELETE, uri)
    new UserRoutes(new UserServiceImpl(userRepositoryImpl)).routes().orNotFound(getHW).unsafeRunSync()
  }
}
